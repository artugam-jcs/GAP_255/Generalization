#include "RpgBattleGame.h"

void RpgBattleGame::Play()
{
    // Build each party
    for (size_t i = 0; i < kTeamCount; ++i)
    {
        m_parties[i].Configure();
    }

    // Battle loop
    for (size_t i = 0; i < kTeamCount; ++i)
    {
        RpgParty& currentParty = m_parties[i];
        RpgParty& opposingParty = m_parties[1 - i];

        currentParty.TakeTurn(opposingParty);
    }
}
