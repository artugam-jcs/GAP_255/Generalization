#pragma once
#include "RpgParty.h"

class RpgBattleGame
{
private:
    static constexpr size_t kTeamCount = 2;
    RpgParty m_parties[kTeamCount];

public:
    void Play();
};