#pragma once

class RpgCharacter;

class RpgParty
{
private:
    static constexpr size_t kPartySize = 2;
    RpgCharacter* m_pCharacters[kPartySize];

public:
    ~RpgParty();
    void Configure();
    void TakeTurn(RpgParty& opposingTeam);
    RpgCharacter* ChooseTarget();
};