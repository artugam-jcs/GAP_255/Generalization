#include "RpgParty.h"
#include <conio.h>
#include <iostream>
#include "RpgCharacter.h"

RpgParty::~RpgParty()
{
    for (size_t i = 0; i < kPartySize; ++i)
    {
        delete m_pCharacters[i];
    }
}

void RpgParty::Configure()
{
    for (size_t i = 0; i < kPartySize; ++i)
    {
        //TODO: print the character options
        std::cout << "Choose your character " << i << ":" << std::endl;
        std::cout << "1) Warrior" << std::endl;
        std::cout << "2) Mage" << std::endl;
        std::cout << "3) Archer" << std::endl;
        std::cout << "4) Thief" << std::endl;

        //TODO: let the user choose
        int chosen;
        std::cin >> chosen;

        RpgCharacter* pCharacter = nullptr;

        switch (chosen)
        {
        case 1: pCharacter = new Warrior(); break;
        case 2: pCharacter = new Mage(); break;
        case 3: pCharacter = new Archer(); break;
        case 4: pCharacter = new Thief(); break;
        }

        m_pCharacters[i] = pCharacter;

        system("cls");
    }
}

void RpgParty::TakeTurn(RpgParty& opposingTeam)
{
    for (size_t i = 0; i < kPartySize; ++i)
    {
        RpgCharacter& target = *opposingTeam.ChooseTarget();

        RpgCharacter* pCharacter = m_pCharacters[i];
        pCharacter->PrintName();

        std::cout << std::endl << std::endl;
        std::cout << "A) Attack\n";
        std::cout << "B) Special 1\n";
        std::cout << "C) Special 2\n";

        char input = _getch();
        switch (input)
        {
        case 'a':
            pCharacter->Attack(target);
            break;
        case 'b':
            pCharacter->Special1(target);
            break;
        case 'c':
            pCharacter->Special2(target);
            break;
        }
    }
}

RpgCharacter* RpgParty::ChooseTarget()
{
    std::cout << "Choose a target:\n";

    std::cout << "1)";
    m_pCharacters[0]->PrintName();
    std::cout << '\n';

    std::cout << "2)";
    m_pCharacters[1]->PrintName();
    std::cout << '\n';

    char input = _getch();
    switch (input)
    {
    case '1': return m_pCharacters[0];
    case '2': return m_pCharacters[1];
    }

    return nullptr;
}
