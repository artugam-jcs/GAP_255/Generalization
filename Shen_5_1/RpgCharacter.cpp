#include "RpgCharacter.h"
#include <iostream>

void RpgCharacter::PrintName()
{
    std::cout << m_name;
}

void RpgCharacter::GotHit(int point)
{
    this->m_health -= point;

    if (m_health < 0)
        m_health = 0;
}

bool RpgCharacter::Cast(int mana)
{
    if (m_mana >= mana)
    {
        m_mana -= mana;
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
// Warrior
///////////////////////////////////////////////////////////////////////////////

void Warrior::Attack(RpgCharacter& target)
{
    target.GotHit(12);
}

void Warrior::Special1(RpgCharacter& target)
{
    const int consume = 10;

    if (Cast(consume))
    {
        target.GotHit(20);
    }
}

void Warrior::Special2(RpgCharacter& target)
{
    const int consume = 20;

    if (Cast(consume))
    {
        target.GotHit(40);
    }
}

///////////////////////////////////////////////////////////////////////////////
// Mage
///////////////////////////////////////////////////////////////////////////////

void Mage::Attack(RpgCharacter& target)
{
    target.GotHit(8);
}

void Mage::Special1(RpgCharacter& target)
{
    const int consume = 5;

    if (Cast(consume))
    {
        target.GotHit(20);
    }
}

void Mage::Special2(RpgCharacter& target)
{
    const int consume = 10;

    if (Cast(consume))
    {
        target.GotHit(40);
    }
}

///////////////////////////////////////////////////////////////////////////////
// Archer
///////////////////////////////////////////////////////////////////////////////

void Archer::Attack(RpgCharacter& target)
{
    target.GotHit(9);
}

void Archer::Special1(RpgCharacter& target)
{
    const int consume = 8;

    if (Cast(consume))
    {
        target.GotHit(20);
    }
}

void Archer::Special2(RpgCharacter& target)
{
    const int consume = 16;

    if (Cast(consume))
    {
        target.GotHit(40);
    }
}

///////////////////////////////////////////////////////////////////////////////
// Thief
///////////////////////////////////////////////////////////////////////////////

void Thief::Attack(RpgCharacter& target)
{
    target.GotHit(12);
}

void Thief::Special1(RpgCharacter& target)
{
    const int consume = 12;

    if (Cast(consume))
    {
        target.GotHit(20);
    }
}

void Thief::Special2(RpgCharacter& target)
{
    const int consume = 12;

    if (Cast(consume))
    {
        target.GotHit(40);
    }
}
