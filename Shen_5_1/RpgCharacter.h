#pragma once
#include <string>

class RpgCharacter
{
protected:
    std::string m_name;
    int m_health;
    int m_mana;

public:
    virtual void Attack(RpgCharacter&) = 0;
    virtual void Special1(RpgCharacter&) = 0;
    virtual void Special2(RpgCharacter&) = 0;
    void GotHit(int);
    bool Cast(int mana);
    void PrintName();
};

class Warrior : public RpgCharacter
{
public:
    Warrior()
    { 
        m_name = "Warrior";
        m_health = 120;
        m_mana = 80;
    }

    virtual void Attack(RpgCharacter&) override;
    virtual void Special1(RpgCharacter&) override;
    virtual void Special2(RpgCharacter&) override;
};

class Mage : public RpgCharacter
{
public:
    Mage()
    {
        m_name = "Mage";
        m_health = 80;
        m_mana = 120;
    }

    virtual void Attack(RpgCharacter&) override;
    virtual void Special1(RpgCharacter&) override;
    virtual void Special2(RpgCharacter&) override;
};

class Archer : public RpgCharacter
{
public:
    Archer()
    {
        m_name = "Archer";
        m_health = 90;
        m_mana = 110;
    }

    virtual void Attack(RpgCharacter&) override;
    virtual void Special1(RpgCharacter&) override;
    virtual void Special2(RpgCharacter&) override;
};

class Thief : public RpgCharacter
{
public:
    Thief()
    {
        m_name = "Thief";
        m_health = 110;
        m_mana = 90;
    }

    virtual void Attack(RpgCharacter&) override;
    virtual void Special1(RpgCharacter&) override;
    virtual void Special2(RpgCharacter&) override;
};
